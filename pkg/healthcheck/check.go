// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package healthcheck

import "time"

type Result struct {
	Name         string   `json:"name"`
	Status       Status   `json:"status"`
	ResponseTime float64  `json:"responseTime"`
	HealthChecks []Result `json:"healthChecks"`
}

type Status string

const StatusOK Status = "OK"
const StatusError Status = "ERROR"

type Checker interface {
	GetHealthCheck() Result
}

func run(name string, items []Checker) Result {
	start := time.Now()
	result := Result{
		Name:         name,
		HealthChecks: make([]Result, len(items)),
	}

	for i, item := range items {
		result.HealthChecks[i] = item.GetHealthCheck()
	}

	result.Status = getStatus(result.HealthChecks)
	result.ResponseTime = time.Since(start).Seconds()

	return result
}

func getStatus(healthChecks []Result) Status {
	for _, healthCheck := range healthChecks {
		if healthCheck.Status != StatusOK {
			return healthCheck.Status
		}
	}

	return StatusOK
}
