// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package healthcheck

import (
	"encoding/json"
	"log"
	"net/http"
)

type Handler struct {
	name         string
	healthChecks []Checker
}

func NewHandler(name string, healthChecks []Checker) *Handler {
	return &Handler{name: name, healthChecks: healthChecks}
}

func (h *Handler) HandleHealth(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *Handler) HandlerHealthCheck(w http.ResponseWriter, r *http.Request) {
	result := run(h.name, h.healthChecks)

	httpStatus := http.StatusOK
	if result.Status != StatusOK {
		httpStatus = http.StatusServiceUnavailable
	}

	w.WriteHeader(httpStatus)

	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
